'use strict';
(function (module, _) {
    try {
        module = angular.module('cwt.include-exclude');
    } catch (e) {
        module = angular.module('cwt.include-exclude', []);
    }
    var directiveName = "setFocus";
    var theDirective = function ($timeout, $parse) {
        return{
            link: function (scope, element, attrs) {
                var model = $parse(attrs.setFocus);
                scope.$watch(model, function(value){
                    if(value){
                        $timeout(function(){
                            element.focus();
                        });
                    }
                });
            }
        }
    }
    theDirective.$inject = ['$timeout', '$parse'];
    module.directive(directiveName, theDirective);
})(null, window._);;'use strict';
(function (module, _) {
    try {
        module = angular.module('cwt.include-exclude');
    } catch (e) {
        module = angular.module('cwt.include-exclude', []);
    }
    var directiveName = "cwtIncludeExclude";
    var theDirective = function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                callback: '&',
                propertyName: '@',
                displayValues: '=?',
                dataModel: '=?',
                onlyInclude: '=?',
                onlyExclude: '=?',
                firstPickSticks: '=?',
                hasError: '=?'
            },
            templateUrl: 'templates/include-exclude.html',
            controllerAs: 'ctrl',
            bindToController: true,
            controller: ['$scope', '$uibModal', function ($scope, $uibModal) {
                var ctrl = this
                if (ctrl.dataModel === undefined) {
                    ctrl.dataModel = [];
                }
                ctrl.itemList = ctrl.dataModel;

                //add items
                ctrl.addIncludedItem = function (item) {
                    item.included = true;
                    ctrl.itemList.push(item);
                }
                ctrl.addExcludedItem = function (item) {
                    item.included = false;
                    ctrl.itemList.push(item);
                }

                ctrl.initialParams = {
                    onlyInclude: angular.copy(ctrl.onlyInclude),
                    onlyExclude: angular.copy(ctrl.onlyExclude)
                };

                ctrl.params = {
                    callback: ctrl.callback,
                    includeItem: ctrl.addIncludedItem,
                    excludeItem: ctrl.addExcludedItem,
                    propertyName: ctrl.propertyName,
                    itemList: ctrl.itemList,
                    onlyInclude: ctrl.onlyInclude,
                    onlyExclude: ctrl.onlyExclude,
                    firstPickSticks: ctrl.firstPickSticks
                };
                //open modal
                ctrl.addItems = function () {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        backdrop: 'static',
                        keyboard: false,
                        templateUrl: 'templates/modalcontent.html',
                        controllerAs: 'ctrl',
                        controller: 'IncludeExcludeModalController',
                        size: 'sm',
                        resolve: {
                            params: function () {
                                return ctrl.params;
                            }
                        }
                    });
                };

                ctrl.removeItem = function (item) {
                    var index = ctrl.itemList.indexOf(item);
                    ctrl.itemList.splice(index, 1);
                    if (ctrl.itemList.length === 0) {
                        ctrl.params.onlyInclude = ctrl.initialParams.onlyInclude;
                        ctrl.params.onlyExclude = ctrl.initialParams.onlyExclude;
                    }
                };

                ctrl.clearAll = function () {
                    ctrl.itemList.length = 0;
                    ctrl.params.onlyInclude = ctrl.initialParams.onlyInclude;
                    ctrl.params.onlyExclude = ctrl.initialParams.onlyExclude;
                }

            }]
        }
    }
    theDirective.$inject = [];
    module.directive(directiveName, theDirective);
})(null, window._);;'use strict';
(function (module) {
    try {
    module = angular.module('cwt.include-exclude');
  } catch (e) {
    module = angular.module('cwt.include-exclude', []);
  }
    var ctrlName = "IncludeExcludeModalController";
    var theController = function ($scope, $uibModalInstance, params, $q) {
        var ctrl = this;
        var promise = [];
        ctrl.loading = false;
        ctrl.onlyInclude = params.onlyInclude;
        ctrl.onlyExclude = params.onlyExclude;
         ctrl.focusInput = true;

        ctrl.includeItems = function (data, include) {
        	if (data != undefined){
        	ctrl.loading = true;
            var lines = data.replace(/\r\n/g, "\n").split("\n");
            promise.push(params.callback({ data: lines }));
            $q.all(promise).then(function (result) {
                var existingItems = angular.copy(result[0]);
                for (var i = lines.length - 1; i >= 0; i--) {
                    for (var j = 0; j < existingItems.length; j++) {
                        if (existingItems[j]) {
                            if (existingItems[j][params.propertyName].replace(/\s/g, '').toLowerCase() == lines[i].replace(/\s/g, '').toLowerCase()) {
                                if (!ctrl.checkIfExists(lines[i])) {
                                    lines.splice(i, 1);
                                    ctrl.input = lines.join("\n");
                                    if (include) {
                                        if(params.firstPickSticks){
                                            params.onlyInclude = true;
                                        }
                                        params.includeItem(existingItems[j]);
                                    }
                                    else {
                                        if(params.firstPickSticks){
                                            params.onlyExclude = true;
                                        }
                                        params.excludeItem(existingItems[j]);
                                    }
                                }
                                else {
                                    lines[i] += " (Already exists)";
                                }
                                break;
                            }
                        }
                    }
                }
                promise = [];
            }).finally(function () {
                ctrl.loading = false;
                if (lines.length === 0) {
                    ctrl.close();
                }
                else {
                    for (var i = 0; i < lines.length; i++) {
                        if (lines[i].indexOf("exists") === -1) {
                            lines[i] += " (Not valid)";
                        }
                    }
                    ctrl.input = lines.join("\n");
                }
            });
        };
        };

        ctrl.checkIfExists = function (item) {
            for (var i = 0; i < params.itemList.length; i++) {
                if (params.itemList[i][params.propertyName].replace(/\s/g, '').toLowerCase() === item.replace(/\s/g, '').toLowerCase()) {
                    return true;
                }
            }
            return false;
        };

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
            ctrl.focusInput = false;
        };

    }
    theController.$inject = ['$scope', '$uibModalInstance', 'params', '$q'];
    module.controller(ctrlName, theController);
})();angular.module('cwt.include-exclude').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('templates/include-exclude.html',
    "<div class=includeexclude-control>\n" +
    "<div class=\"includeexclude-control form-control\" ng-class=\"{'has-error': ctrl.hasError}\">\n" +
    "<div ng-repeat=\"item in ctrl.itemList\" class=\"includeexclude-badge form-control\" ng-class=\"item.included ? 'isIncluded' :'isExcluded'\">\n" +
    "<span ng-if=!ctrl.displayValues>{{item[ctrl.propertyName]}}</span>\n" +
    "<span ng-if=ctrl.displayValues>\n" +
    "<span ng-repeat-start=\"propertyName in ctrl.displayValues\" ng-if=$first>{{item[propertyName]}}</span>\n" +
    "<span ng-repeat-end ng-if=!$first>-&nbsp;{{item[propertyName]}}</span>\n" +
    "</span>\n" +
    "<i class=\"fa fa-times\" ng-click=ctrl.removeItem(item)></i>\n" +
    "</div>\n" +
    "</div>\n" +
    "<div class=\"btn-group margin-top\" style=width:100%>\n" +
    "<button type=button class=\"btn btn-primary\" ng-click=ctrl.addItems()>Add</button>\n" +
    "<button type=button class=\"btn btn-default\" ng-click=ctrl.clearAll()>Clear all</button>\n" +
    "</div>\n" +
    "</div>"
  );


  $templateCache.put('templates/modalcontent.html',
    "<div id=includeexclude>\n" +
    "<div class=\"modal-header include-exclude-modal-header\">\n" +
    "<a>\n" +
    "<i ng-click=ctrl.close() class=\"fa fa-times pull-right\"></i>\n" +
    "</a>\n" +
    "<div class=include-exclude-modal-control>\n" +
    "<textarea class=form-control rows=5 placeholder=\"Paste or write your input here.\" ng-model=ctrl.input set-focus=ctrl.focusInput ng-disabled=ctrl.loading></textarea>\n" +
    "</div>\n" +
    "<div class=btn-group>\n" +
    "<button type=button class=\"btn btn-primary\" ng-click=\"ctrl.includeItems(ctrl.input, true)\" ng-hide=ctrl.onlyExclude ng-disabled=ctrl.loading ng-class=\"{'only-include': ctrl.onlyInclude}\">\n" +
    "<span ng-show=ctrl.loading>\n" +
    "<i class=\"fa fa-circle-o-notch fa-spin\" style=\"font-size: 16px\"></i>\n" +
    "</span>\n" +
    "Include\n" +
    "</button>\n" +
    "<button type=button class=\"btn btn-danger\" ng-click=\"ctrl.includeItems(ctrl.input, false)\" ng-hide=ctrl.onlyInclude ng-disabled=ctrl.loading ng-class=\"{'only-exclude': ctrl.onlyExclude}\">\n" +
    "<span ng-show=ctrl.loading>\n" +
    "<i class=\"fa fa-circle-o-notch fa-spin\" style=\"font-size: 16px\"></i>\n" +
    "</span>\n" +
    "Exclude\n" +
    "</button>\n" +
    "</div>\n" +
    "</div>\n" +
    "</div>"
  );

}]);
