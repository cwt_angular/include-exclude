'use strict';
(function (module) {
    try {
    module = angular.module('cwt.include-exclude');
  } catch (e) {
    module = angular.module('cwt.include-exclude', []);
  }
    var ctrlName = "IncludeExcludeModalController";
    var theController = function ($scope, $uibModalInstance, params, $q) {
        var ctrl = this;
        var promise = [];
        ctrl.loading = false;
        ctrl.onlyInclude = params.onlyInclude;
        ctrl.onlyExclude = params.onlyExclude;
         ctrl.focusInput = true;

        ctrl.includeItems = function (data, include) {
        	if (data != undefined){
        	ctrl.loading = true;
            var lines = data.replace(/\r\n/g, "\n").split("\n");
            promise.push(params.callback({ data: lines }));
            $q.all(promise).then(function (result) {
                var existingItems = angular.copy(result[0]);
                for (var i = lines.length - 1; i >= 0; i--) {
                    for (var j = 0; j < existingItems.length; j++) {
                        if (existingItems[j]) {
                            if (existingItems[j][params.propertyName].replace(/\s/g, '').toLowerCase() == lines[i].replace(/\s/g, '').toLowerCase()) {
                                if (!ctrl.checkIfExists(lines[i])) {
                                    lines.splice(i, 1);
                                    ctrl.input = lines.join("\n");
                                    if (include) {
                                        if(params.firstPickSticks){
                                            params.onlyInclude = true;
                                        }
                                        params.includeItem(existingItems[j]);
                                    }
                                    else {
                                        if(params.firstPickSticks){
                                            params.onlyExclude = true;
                                        }
                                        params.excludeItem(existingItems[j]);
                                    }
                                }
                                else {
                                    lines[i] += " (Already exists)";
                                }
                                break;
                            }
                        }
                    }
                }
                promise = [];
            }).finally(function () {
                ctrl.loading = false;
                if (lines.length === 0) {
                    ctrl.close();
                }
                else {
                    for (var i = 0; i < lines.length; i++) {
                        if (lines[i].indexOf("exists") === -1) {
                            lines[i] += " (Not valid)";
                        }
                    }
                    ctrl.input = lines.join("\n");
                }
            });
        };
        };

        ctrl.checkIfExists = function (item) {
            for (var i = 0; i < params.itemList.length; i++) {
                if (params.itemList[i][params.propertyName].replace(/\s/g, '').toLowerCase() === item.replace(/\s/g, '').toLowerCase()) {
                    return true;
                }
            }
            return false;
        };

        ctrl.close = function () {
            $uibModalInstance.dismiss('cancel');
            ctrl.focusInput = false;
        };

    }
    theController.$inject = ['$scope', '$uibModalInstance', 'params', '$q'];
    module.controller(ctrlName, theController);
})()