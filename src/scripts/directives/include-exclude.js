'use strict';
(function (module, _) {
    try {
        module = angular.module('cwt.include-exclude');
    } catch (e) {
        module = angular.module('cwt.include-exclude', []);
    }
    var directiveName = "cwtIncludeExclude";
    var theDirective = function () {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                callback: '&',
                propertyName: '@',
                displayValues: '=?',
                dataModel: '=?',
                onlyInclude: '=?',
                onlyExclude: '=?',
                firstPickSticks: '=?',
                hasError: '=?'
            },
            templateUrl: 'templates/include-exclude.html',
            controllerAs: 'ctrl',
            bindToController: true,
            controller: ['$scope', '$uibModal', function ($scope, $uibModal) {
                var ctrl = this
                if (ctrl.dataModel === undefined) {
                    ctrl.dataModel = [];
                }
                ctrl.itemList = ctrl.dataModel;

                //add items
                ctrl.addIncludedItem = function (item) {
                    item.included = true;
                    ctrl.itemList.push(item);
                }
                ctrl.addExcludedItem = function (item) {
                    item.included = false;
                    ctrl.itemList.push(item);
                }

                ctrl.initialParams = {
                    onlyInclude: angular.copy(ctrl.onlyInclude),
                    onlyExclude: angular.copy(ctrl.onlyExclude)
                };

                ctrl.params = {
                    callback: ctrl.callback,
                    includeItem: ctrl.addIncludedItem,
                    excludeItem: ctrl.addExcludedItem,
                    propertyName: ctrl.propertyName,
                    itemList: ctrl.itemList,
                    onlyInclude: ctrl.onlyInclude,
                    onlyExclude: ctrl.onlyExclude,
                    firstPickSticks: ctrl.firstPickSticks
                };
                //open modal
                ctrl.addItems = function () {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        backdrop: 'static',
                        keyboard: false,
                        templateUrl: 'templates/modalcontent.html',
                        controllerAs: 'ctrl',
                        controller: 'IncludeExcludeModalController',
                        size: 'sm',
                        resolve: {
                            params: function () {
                                return ctrl.params;
                            }
                        }
                    });
                };

                ctrl.removeItem = function (item) {
                    var index = ctrl.itemList.indexOf(item);
                    ctrl.itemList.splice(index, 1);
                    if (ctrl.itemList.length === 0) {
                        ctrl.params.onlyInclude = ctrl.initialParams.onlyInclude;
                        ctrl.params.onlyExclude = ctrl.initialParams.onlyExclude;
                    }
                };

                ctrl.clearAll = function () {
                    ctrl.itemList.length = 0;
                    ctrl.params.onlyInclude = ctrl.initialParams.onlyInclude;
                    ctrl.params.onlyExclude = ctrl.initialParams.onlyExclude;
                }

            }]
        }
    }
    theDirective.$inject = [];
    module.directive(directiveName, theDirective);
})(null, window._);