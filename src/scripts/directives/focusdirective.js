'use strict';
(function (module, _) {
    try {
        module = angular.module('cwt.include-exclude');
    } catch (e) {
        module = angular.module('cwt.include-exclude', []);
    }
    var directiveName = "setFocus";
    var theDirective = function ($timeout, $parse) {
        return{
            link: function (scope, element, attrs) {
                var model = $parse(attrs.setFocus);
                scope.$watch(model, function(value){
                    if(value){
                        $timeout(function(){
                            element.focus();
                        });
                    }
                });
            }
        }
    }
    theDirective.$inject = ['$timeout', '$parse'];
    module.directive(directiveName, theDirective);
})(null, window._);